#version 450

in vec3 vPosition;
in vec4 vColor;

out vec4 fColor;

void main()
{
    // Pick a coordinate to visualize in a grid
    vec2 coord = vPosition.xy;

    // Compute anti-aliased world-space grid lines
    vec2 grid = vec2(1e9);
    grid = abs(fract(coord - 0.5) - 0.5) / fwidth(coord);

    float line = min(grid.x, grid.y);
    vec4 gridColor = vec4(vec3(1.0 - min(line, 1.0)), 1.0);

    fColor = mix(vColor, gridColor, 0.1);
}