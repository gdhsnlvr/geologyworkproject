#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 color;
layout(location = 2) in vec3 normal;

out vec3 vPosition;
out vec4 vColor;

uniform mat4 modelToWorld;
uniform mat4 worldToView;

vec4 lightPosition = vec4(0, 0, 10, 1);
vec3 lightCoefficient = vec3(1, 1, 1);
vec3 lightIntensive = vec3(1, 1, 1);

void main() {
    mat3 normalMatrix = transpose(inverse(mat3(modelToWorld)));
    vec3 vNormal = normalize(normalMatrix * normal);
    vec4 eyeCoords = modelToWorld * vec4(position, 1.0);
    vec3 s = normalize(vec3(lightPosition - eyeCoords));
    vColor = vec4(lightIntensive * lightCoefficient * max( dot(s, vNormal), 0.0 ) * color, 1.0);

    vPosition = position;
    gl_Position = worldToView * modelToWorld * vec4(vPosition, 1.0);
}
