set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

cmake_minimum_required(VERSION 3.6)
project(geologyWorkProject)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -O0")

set(SOURCE_FILES source/main.cpp source/geometry/Real.cpp source/geometry/Real.hpp
        source/geometry/vec3.cpp source/geometry/vec3.h source/geometry/vec2.cpp source/geometry/vec2.h source/geometry/mesh.cpp source/geometry/mesh.h source/geometry/triangle.cpp source/geometry/triangle.h source/geometry/vertex.cpp source/geometry/vertex.h source/geometry/segment.cpp source/geometry/segment.h source/geometry/line.cpp source/geometry/line.h source/geometry/geometry.cpp source/geometry/geometry.h source/geometry/detail/geometry_detail.cpp source/geometry/detail/geometry_detail.h source/geometry/plane.cpp source/geometry/plane.h source/geometry/io/io.cpp source/geometry/io/io.h source/geometry/io/detail/geometry_io_detail.cpp source/geometry/io/detail/geometry_io_detail.h source/geometry/gpc/gpc.cpp source/geometry/gpc/gpc.h source/geometry/polygon.cpp source/geometry/polygon.h source/deposit/Domain.cpp source/deposit/Domain.h source/deposit/Region.cpp source/deposit/Region.h source/deposit/Horizon.cpp source/deposit/Horizon.h source/deposit/Deposit.cpp source/deposit/Deposit.h source/deposit/io/deposit_io.cpp source/deposit/io/deposit_io.h source/geometry/horizon_rastarization.cpp source/geometry/horizon_rastarization.h source/geometry/raster.cpp source/geometry/raster.h source/utils/TimeTracker.cpp source/utils/TimeTracker.h source/geometry/mapped_mesh.cpp source/geometry/mapped_mesh.h)
add_executable(geologyWorkProject ${SOURCE_FILES} resource/resource.qrc)
include_directories(.)