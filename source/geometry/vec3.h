//
// Created by gdhsnlvr on 18.09.16.
//

#ifndef GEOLOGYWORKPROJECT_VEC3_H
#define GEOLOGYWORKPROJECT_VEC3_H

#include "Real.hpp"

namespace geometry {
    class vec3 {
    public:
        class vec2 {
        public:
            vec2();
            vec2(real_t xy);
            vec2(real_t x, real_t y);
            vec2(const vec3 &v);

            vec2 operator - (const vec2 &v) const;
            vec2 operator + (const vec2 &v) const;
            vec2 operator * (real_t t) const;
            vec2 operator / (real_t t) const;

            static real_t dot(const vec2 &v1, const vec2 &v2);
            static real_t cross(const vec2 &v1, const vec2 &v2);
            static real_t length(const vec2 &v);
            static vec2 componentMax(const vec2 &v1, const vec2 &v2);
            static vec2 componentMin(const vec2 &v1, const vec2 &v2);
            static vec2 normalized(const vec2 &v);
            static vec2 normal(const vec2 &v1, const vec2 &v2);

        public:
            real_t x, y;
        };

    public:
        vec3();
        vec3(real_t xyz);
        vec3(real_t x, real_t y, real_t z);
        vec3(const vec2 &v, real_t z);

        vec3 operator - (const vec3 &v) const;
        vec3 operator + (const vec3 &v) const;
        vec3 operator * (real_t t) const;
        vec3 operator / (real_t t) const;

        static real_t dot(const vec3 &v1, const vec3 &v2);
        static real_t length(const vec3 &v);
        static vec3 cross(const vec3 &v1, const vec3 &v2);
        static vec3 componentMax(const vec3 &v1, const vec3 &v2);
        static vec3 componentMin(const vec3 &v1, const vec3 &v2);
        static vec3 normalized(const vec3 &v);
        static vec3 normal(const vec3 &v1, const vec3 &v2, const vec3 &v3);

    public:
        real_t x, y, z;
    };
}


#endif //GEOLOGYWORKPROJECT_VEC3_H
