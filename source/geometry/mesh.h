//
// Created by gdhsnlvr on 18.09.16.
//

#ifndef GEOLOGYWORKPROJECT_MESH_H
#define GEOLOGYWORKPROJECT_MESH_H

#include <vector>

#include "triangle.h"
#include "segment.h"

namespace geometry {
    class mesh {
    protected:
        std::vector<geometry::triangle> m_triangles;
        geometry::vec3 m_minBoundary;
        geometry::vec3 m_maxBoundary;

    public:
        mesh() = default;
        mesh(const std::vector<geometry::triangle> triangles);

        const std::vector<geometry::triangle> &triangles() const;
        std::vector<geometry::triangle> &r_triangles();

        const geometry::vec3 &minBoundary() const;
        const geometry::vec3 &maxBoundary() const;

        void updateBoundary();
    };
}

#endif //GEOLOGYWORKPROJECT_MESH_H
