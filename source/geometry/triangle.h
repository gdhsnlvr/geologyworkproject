//
// Created by gdhsnlvr on 18.09.16.
//

#ifndef GEOLOGYWORKPROJECT_TRIANGLE_H
#define GEOLOGYWORKPROJECT_TRIANGLE_H

#include <array>
#include "vertex.h"

namespace geometry {
    class triangle {
    protected:
        std::array<vertex, 3> m_vertexes;

    public:
        triangle() = default;
        triangle(const vertex &v1, const vertex &v2, const vertex &v3);

        const vertex &operator [] (std::size_t index) const;
        vertex &operator [] (std::size_t index);

        std::array<vertex, 3>::const_iterator begin() const;
        std::array<vertex, 3>::const_iterator end() const;
        std::array<vertex, 3>::iterator begin();
        std::array<vertex, 3>::iterator end();
    };
}


#endif //GEOLOGYWORKPROJECT_TRIANGLE_H
