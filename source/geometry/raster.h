//
// Created by gdhsnlvr on 03.10.16.
//

#ifndef GEOLOGYWORKPROJECT_RASTER_H
#define GEOLOGYWORKPROJECT_RASTER_H

#include "Real.hpp"

namespace geometry
{
    class raster
    {
    protected:
        real_t m_stoneVolume;

    public:
        raster();

        real_t stoneVolume() const;
        void setStoneVolume(real_t volume);
    };
}


#endif //GEOLOGYWORKPROJECT_RASTER_H
