//
// Created by gdh on 24.07.16.
//


#include <cmath>

#include "Real.hpp"

bool Real::fizzyEqual(real_t a, real_t b) {
    return std::abs(a - b) < epsilon;
}

bool Real::fizzyLess(real_t a, real_t b) {
    return a < b + epsilon;
}

bool Real::fizzyGreater(real_t a, real_t b) {
    return a + epsilon > b;
}

real_t Real::abs(real_t x)
{
    return x >= 0 ? x : - x;
}

real_t Real::max(real_t a, real_t b)
{
    return a > b ? a : b;
}

real_t Real::min(real_t a, real_t b)
{
    return a < b ? a : b;
}
