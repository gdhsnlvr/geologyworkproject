//
// Created by gdhsnlvr on 18.09.16.
//

#ifndef GEOLOGYWORKPROJECT_SEGMENT_H
#define GEOLOGYWORKPROJECT_SEGMENT_H

#include "vec2.h"

namespace geometry {
    class segment {
    public:
        enum class type {
            Interval,
            RightOpenInterval,
            LeftOpenInterval,
            Segment
        };

    protected:
        vec2 m_begin;
        vec2 m_end;
        type m_type;

    public:
        segment() = default;
        segment(const vec2 &begin, const vec2 &end, type _type = type::Segment);

        const vec2 &begin() const;
        const vec2 &end() const;

        static bool correctParamter(const segment &s, real_t parameter);
    };
}


#endif //GEOLOGYWORKPROJECT_SEGMENT_H
