//
// Created by gdhsnlvr on 18.09.16.
//

#ifndef GEOLOGYWORKPROJECT_VEC2_H
#define GEOLOGYWORKPROJECT_VEC2_H

#include <source/geometry/vec3.h>

namespace geometry {
    using vec2 = vec3::vec2;
}

#endif //GEOLOGYWORKPROJECT_VEC2_H
