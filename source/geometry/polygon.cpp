//
// Created by gdhsnlvr on 24.09.16.
//

#include <source/geometry/gpc/gpc.h>
#include "polygon.h"

geometry::polygon::polygon(const geometry::polygon::contour_t &contour)
{
    m_polygon.contour = nullptr;
    m_polygon.hole = nullptr;
    m_polygon.num_contours = 0;

    gpc_vertex_list vertex_list;
    vertex_list.num_vertices = (int) contour.size();
    vertex_list.vertex = new gpc_vertex[contour.size()];
    for (int i = 0; i < contour.size(); i++) {
        vertex_list.vertex[i].x = contour[i].x;
        vertex_list.vertex[i].y = contour[i].y;
    }
    gpc_add_contour(&m_polygon, &vertex_list, 0);
}

int geometry::polygon::contourCount() const
{
    return m_polygon.num_contours;
}

geometry::polygon::contour_t geometry::polygon::getContour(int contourIndex) const
{
    return convert(m_polygon.contour[contourIndex]);
}

geometry::polygon::contour_t geometry::polygon::convert(const gpc_vertex_list &vertex_list) const
{
    std::vector<geometry::vec2> contour;
    for (int i = 0; i < vertex_list.num_vertices; i++) {
        contour.emplace_back(vertex_list.vertex[i].x, vertex_list.vertex[i].y);
    }
    return contour;
}

geometry::polygon::polygon(const gpc_polygon &polygon)
{
    m_polygon = polygon;
}

geometry::polygon geometry::polygon::intersect(geometry::polygon &polygon)
{
    gpc_polygon result;
    gpc_polygon_clip(GPC_INT, &m_polygon, &polygon.m_polygon, &result);
    return geometry::polygon(result);
}

geometry::polygon geometry::polygon::difference(geometry::polygon &polygon)
{
    gpc_polygon result;
    gpc_polygon_clip(GPC_DIFF, &m_polygon, &polygon.m_polygon, &result);
    return geometry::polygon(result);
}

geometry::polygon geometry::polygon::unite(geometry::polygon &polygon)
{
    gpc_polygon result;
    gpc_polygon_clip(GPC_UNION, &m_polygon, &polygon.m_polygon, &result);
    return geometry::polygon(result);
}
