//
// Created by gdhsnlvr on 01.10.16.
//

#ifndef GEOLOGYWORKPROJECT_RASTARIZATION_H
#define GEOLOGYWORKPROJECT_RASTARIZATION_H

#include <vector>
#include "raster.h"

namespace geometry
{
    class horizon_rastarization
    {
    public:
        using row_t = std::vector<geometry::raster>;
        using table_t = std::vector<row_t>;

    protected:
        table_t m_rasters;

    public:
        horizon_rastarization();

        const table_t &rasters() const;
        table_t &r_rasters();
    };
}


#endif //GEOLOGYWORKPROJECT_RASTARIZATION_H
