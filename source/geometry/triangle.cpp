//
// Created by gdhsnlvr on 18.09.16.
//

#include "triangle.h"

geometry::triangle::triangle(const geometry::vertex &v1, const geometry::vertex &v2, const geometry::vertex &v3)
{
    m_vertexes[0] = v1;
    m_vertexes[1] = v2;
    m_vertexes[2] = v3;
}

const geometry::vertex &geometry::triangle::operator[](std::size_t index) const {
    return m_vertexes[index];
}

geometry::vertex &geometry::triangle::operator[](std::size_t index) {
    return m_vertexes[index];
}

std::array<geometry::vertex, 3>::const_iterator geometry::triangle::begin() const {
    return m_vertexes.begin();
}

std::array<geometry::vertex, 3>::const_iterator geometry::triangle::end() const {
    return m_vertexes.end();
}

std::array<geometry::vertex, 3>::iterator geometry::triangle::begin() {
    return m_vertexes.begin();
}

std::array<geometry::vertex, 3>::iterator geometry::triangle::end() {
    return m_vertexes.end();
}
