//
// Created by gdhsnlvr on 19.09.16.
//

#ifndef GEOLOGYWORKPROJECT_PLANE_H
#define GEOLOGYWORKPROJECT_PLANE_H

#include "vec3.h"

namespace geometry
{
    class plane
    {
    protected:
        geometry::vec3 m_normal;
        real_t m_constant;

    public:
        plane() = default;
        plane(const geometry::vec3 &normal, real_t constant);
        plane(const geometry::vec3 &a, const geometry::vec3 &b, const geometry::vec3 &c);

        const geometry::vec3 &normal() const;
        real_t constant() const;
    };
}


#endif //GEOLOGYWORKPROJECT_PLANE_H
