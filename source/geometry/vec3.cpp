//
// Created by gdhsnlvr on 18.09.16.
//

#include <algorithm>
#include "vec3.h"

geometry::vec3::vec3()
        : x(0), y(0), z(0)
{
}

geometry::vec3::vec3(real_t xyz)
        : x(xyz), y(xyz), z(xyz)
{
}

geometry::vec3::vec3(real_t x, real_t y, real_t z)
        : x(x), y(y), z(z)
{
}

geometry::vec3::vec3(const geometry::vec3::vec2 &v, real_t z)
        : x(v.x), y(v.y), z(z)
{

}

geometry::vec3 geometry::vec3::operator-(const geometry::vec3 &v) const {
    return geometry::vec3(x - v.x, y - v.y, z - v.z);
}

geometry::vec3 geometry::vec3::operator+(const geometry::vec3 &v) const {
    return geometry::vec3(x + v.x, y + v.y, z + v.z);
}

geometry::vec3 geometry::vec3::operator*(real_t t) const {
    return geometry::vec3(x * t, y * t, z * t);
}

geometry::vec3 geometry::vec3::operator/(real_t t) const {
    return geometry::vec3(x / t, y / t, z / t);
}

real_t geometry::vec3::dot(const geometry::vec3 &v1, const geometry::vec3 &v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

geometry::vec3 geometry::vec3::cross(const geometry::vec3 &v1, const geometry::vec3 &v2) {
    return geometry::vec3(
            v1.y * v2.z - v1.z * v2.y,
            - (v1.x * v2.z - v1.z * v2.x),
            v1.x * v2.y - v1.y * v2.x
    );
}

geometry::vec3 geometry::vec3::componentMax(const geometry::vec3 &v1, const geometry::vec3 &v2) {
    real_t x = std::max(v1.x, v2.x);
    real_t y = std::max(v1.y, v2.y);
    real_t z = std::max(v1.z, v2.z);
    return geometry::vec3(x, y, z);
}

geometry::vec3 geometry::vec3::componentMin(const geometry::vec3 &v1, const geometry::vec3 &v2) {
    real_t x = std::min(v1.x, v2.x);
    real_t y = std::min(v1.y, v2.y);
    real_t z = std::min(v1.z, v2.z);
    return geometry::vec3(x, y, z);
}

geometry::vec3 geometry::vec3::normal(const geometry::vec3 &v1, const geometry::vec3 &v2, const geometry::vec3 &v3) {
    return geometry::vec3::normalized(geometry::vec3::cross(v2 - v1, v3 - v2));
}

real_t geometry::vec3::length(const geometry::vec3 &v)
{
    return std::sqrt(geometry::vec3::dot(v, v));
}

geometry::vec3 geometry::vec3::normalized(const geometry::vec3 &v)
{
    return v / geometry::vec3::length(v);
}
