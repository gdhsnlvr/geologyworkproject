//
// Created by gdhsnlvr on 18.09.16.
//

#ifndef GEOLOGYWORKPROJECT_GEOMETRY_DETAIL_H
#define GEOLOGYWORKPROJECT_GEOMETRY_DETAIL_H

#include <source/geometry/mesh.h>
#include <source/geometry/line.h>
#include <source/geometry/segment.h>
#include <source/geometry/plane.h>
#include <source/geometry/horizon_rastarization.h>
#include <source/deposit/Horizon.h>
#include <source/geometry/mapped_mesh.h>

namespace geometry
{
    namespace detail
    {
        real_t distance(const geometry::vec2 &v1, const geometry::vec2 &v2);
        real_t distance(const geometry::vec2 &v,  const geometry::segment &segment);
        real_t distance(const geometry::vec3 &v1, const geometry::vec3 &v2);

        real_t square(const geometry::triangle &triangle);

        real_t heightAt(const geometry::vec2 &position, const geometry::plane &plane);
        real_t heightAt(const geometry::vec2 &position, const geometry::triangle &triangle);
        real_t heightAt(const geometry::vec2 &position, const geometry::mesh &mesh);
        real_t heightAtSubSet(const geometry::vec2 &position, const geometry::mesh &mesh, const std::vector<int> &subset);

        geometry::vec2 normal(const geometry::line<geometry::vec2> &line);
        geometry::vec2 normal(const geometry::segment &segment);
        geometry::vec3 normal(const geometry::plane &plane);
        geometry::vec3 normal(const geometry::triangle &triangle);

        geometry::vec2 center(const geometry::segment &segment);
        geometry::vec3 center(const geometry::triangle &triangle);

        bool intersect(const geometry::line<geometry::vec2> &l1, const geometry::line<geometry::vec2> &l2, std::pair<real_t, real_t> &parameterPack);
        bool intersect(const geometry::line<geometry::vec2> &l1, const geometry::line<geometry::vec2> &l2, vec2 &result);
        bool intersect(const geometry::segment &s1, const geometry::segment &s2, std::pair<real_t, real_t> &parameterPack);
        bool intersect(const geometry::segment &s1, const geometry::segment &s2, vec2 &result);

        bool belongs(const geometry::vec3 &v, const geometry::plane &plane);
        bool belongs(const geometry::vec3 &v, const geometry::triangle &triangle);

        void split_mesh(geometry::mesh &mesh, real_t height);
        void split_mesh(geometry::mesh &mesh, const geometry::segment &segment);
        geometry::mesh slice_mesh(const geometry::mesh &mesh, real_t from, real_t to);

        real_t rayCasting(const geometry::plane &plane, const geometry::line<geometry::vec3> &ray);
        real_t rayCasting(const geometry::triangle &triangle, const geometry::line<geometry::vec3> &ray);
        real_t rayCasting(const geometry::mesh &mesh, const geometry::line<geometry::vec3> &ray);

        geometry::mapped_mesh mapMesh(const geometry::mesh &mesh, const geometry::vec2 &size);
        geometry::horizon_rastarization rastarizate(const Horizon &horizon, const geometry::vec2 &size, int detalization);

        template <typename point_t>
        point_t transform(const geometry::vec2 &vec)
        {
            return point_t(vec.x, vec.y);
        }

        template <typename point_t>
        point_t transform(const geometry::vec3 &vec)
        {
            return point_t(vec.x, vec.y, vec.z);
        }
    }
}


#endif //GEOLOGYWORKPROJECT_GEOMETRY_DETAIL_H
