//
// Created by gdhsnlvr on 19.09.16.
//

#ifndef GEOLOGYWORKPROJECT_IO_H
#define GEOLOGYWORKPROJECT_IO_H

#include <string>
#include <fstream>

#include <source/geometry/mesh.h>
#include <source/geometry/io/detail/geometry_io_detail.h>

namespace geometry
{
    namespace io
    {
        template <typename T>
        T read(const std::string &fileName)
        {
            std::ifstream ifs(fileName, std::ios_base::binary);

            T temp;
            geometry::io::detail::read(temp, ifs);
            return temp;
        }


        template <typename T>
        void read(const T &t, const std::string &fileName)
        {
            std::ofstream ofs(fileName, std::ios_base::binary);
            geometry::io::detail::write(t, ofs);
        }
    }
}


#endif //GEOLOGYWORKPROJECT_IO_H
