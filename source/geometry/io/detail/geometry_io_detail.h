//
// Created by gdhsnlvr on 19.09.16.
//

#ifndef GEOLOGYWORKPROJECT_GEOMETRY_IO_DETAIL_H
#define GEOLOGYWORKPROJECT_GEOMETRY_IO_DETAIL_H


#include <source/geometry/vec2.h>
#include <source/geometry/mesh.h>
#include <fstream>

namespace geometry
{
    namespace io
    {
        namespace detail
        {
            template <typename T>
            void read(T &value, std::ifstream &ifs)
            {
                ifs.read(reinterpret_cast<char *>(&value), sizeof(T));
            }

            template <typename T>
            void write(const T &value, std::ofstream &ofs)
            {
                ofs.write(reinterpret_cast<const char *>(&value), sizeof(T));
            }

            void read(geometry::vec2 &vec, std::ifstream &ifs);
            void read(geometry::vec3 &vec, std::ifstream &ifs);
            void read(geometry::vertex &vertex, std::ifstream &ifs);
            void read(geometry::triangle &triangle, std::ifstream &ifs);
            void read(geometry::mesh &mesh, std::ifstream &ifs);

            void write(const geometry::vec2 &vec, std::ofstream &ofs);
            void write(const geometry::vec3 &vec, std::ofstream &ofs);
            void write(const geometry::vertex &vertex, std::ofstream &ofs);
            void write(const geometry::triangle &triangle, std::ofstream &ofs);
            void write(const geometry::mesh &mesh, std::ofstream &ofs);
        }
    }
}


#endif //GEOLOGYWORKPROJECT_GEOMETRY_IO_DETAIL_H
