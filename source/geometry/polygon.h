//
// Created by gdhsnlvr on 24.09.16.
//

#ifndef GEOLOGYWORKPROJECT_POLYGON_H
#define GEOLOGYWORKPROJECT_POLYGON_H

#include <source/geometry/gpc/gpc.h>
#include <vector>
#include "vec2.h"

namespace geometry
{
    class polygon
    {
    protected:
        gpc_polygon m_polygon;

    public:
        using contour_t = std::vector<geometry::vec2>;

        polygon() = default;
        polygon(const contour_t &contour);

        int contourCount() const;
        contour_t getContour(int contourIndex) const;

        geometry::polygon intersect(geometry::polygon &polygon);
        geometry::polygon difference(geometry::polygon &polygon);
        geometry::polygon unite(geometry::polygon &polygon);

    protected:
        polygon(const gpc_polygon &polygon);
        contour_t convert(const gpc_vertex_list &vertex_list) const;
    };
}


#endif //GEOLOGYWORKPROJECT_POLYGON_H