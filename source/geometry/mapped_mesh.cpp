//
// Created by gdhsnlvr on 04.10.16.
//

#include "mapped_mesh.h"

const std::vector<std::vector<std::vector<int> > > &geometry::mapped_mesh::map() const
{
    return m_map;
}

std::vector<std::vector<std::vector<int> > > &geometry::mapped_mesh::r_map()
{
    return m_map;
}
