//
// Created by gdhsnlvr on 18.09.16.
//

#include <algorithm>
#include "vec2.h"

geometry::vec3::vec2::vec2()
        : x(0), y(0)
{
}

geometry::vec3::vec2::vec2(real_t xy)
        : x(xy), y(xy)
{
}

geometry::vec3::vec2::vec2(real_t x, real_t y)
        : x(x), y(y)
{
}

geometry::vec3::vec2::vec2(const geometry::vec3 &v)
        : x(v.x), y(v.y)
{
}

geometry::vec3::vec2 geometry::vec3::vec2::operator-(const geometry::vec2 &v) const {
    return geometry::vec3::vec2(x - v.x, y - v.y);
}

geometry::vec3::vec2 geometry::vec3::vec2::operator+(const geometry::vec2 &v) const {
    return geometry::vec3::vec2(x + v.x, y + v.y);
}

geometry::vec3::vec2 geometry::vec3::vec2::operator*(real_t t) const {
    return geometry::vec3::vec2(x * t, y * t);
}

geometry::vec3::vec2 geometry::vec3::vec2::operator/(real_t t) const {
    return geometry::vec3::vec2(x / t, y / t);
}

real_t geometry::vec3::vec2::dot(const geometry::vec3::vec2 &v1, const geometry::vec3::vec2 &v2) {
    return v1.x * v2.x + v1.y * v2.y;
}

real_t geometry::vec3::vec2::cross(const geometry::vec3::vec2 &v1, const geometry::vec3::vec2 &v2) {
    return v1.x * v2.y - v1.y * v2.x;
}

geometry::vec3::vec2 geometry::vec3::vec2::componentMax(const geometry::vec3::vec2 &v1, const geometry::vec3::vec2 &v2) {
    real_t x = std::max(v1.x, v2.x);
    real_t y = std::max(v1.y, v2.y);
    return geometry::vec2(x, y);
}

geometry::vec3::vec2 geometry::vec3::vec2::componentMin(const geometry::vec3::vec2 &v1, const geometry::vec3::vec2 &v2) {
    real_t x = std::min(v1.x, v2.x);
    real_t y = std::min(v1.y, v2.y);
    return geometry::vec2(x, y);
}

geometry::vec3::vec2 geometry::vec3::vec2::normal(const geometry::vec3::vec2 &v1, const geometry::vec3::vec2 &v2) {
    geometry::vec2 delta = v2 - v1;
    return geometry::vec2::normalized(geometry::vec2(- delta.y, delta.x));
}

real_t geometry::vec3::vec2::length(const geometry::vec3::vec2 &v)
{
    return std::sqrt(geometry::vec2::dot(v, v));
}

geometry::vec3::vec2 geometry::vec3::vec2::normalized(const geometry::vec3::vec2 &v)
{
    return v / geometry::vec2::length(v);
}
