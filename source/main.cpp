#include <source/geometry/mesh.h>
#include <source/geometry/io/io.h>
#include <source/geometry/detail/geometry_detail.h>

#include <iostream>

#include <source/deposit/Deposit.h>
#include <source/utils/TimeTracker.h>

int main(int argc, char **args) {
    geometry::mesh mesh = geometry::io::read<geometry::mesh>("/home/gdhsnlvr/surface3.mesh");
    std::vector<real_t> heights = {1, 1.5, 1.7, 1.9, 2.4, 2.7, 2.9, 3.1};
    Deposit *deposit = new Deposit(mesh, heights);
    auto rastarization = geometry::detail::rastarizate(*deposit->horizons()[4], geometry::vec2(0.25, 0.25), 3);

    std::cout.setf(std::ios::fixed | std::ios::showpoint);
    std::cout.precision(2);
    for (auto row : rastarization.rasters()) {
        for (auto raster : row) {
            std::cout << (raster.stoneVolume() == 0 ? "." : "#") << "   ";
        }
        std::cout << std::endl;
    }

    return 0;
}