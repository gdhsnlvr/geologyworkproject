//
// Created by gdhsnlvr on 04.10.16.
//

#include <iostream>
#include "TimeTracker.h"

TimeTracker &TimeTracker::get()
{
    static TimeTracker tracker;
    return tracker;
}

void TimeTracker::pushTimer(std::string file, std::string function, int line)
{
    m_timeStamps.push(
            {file, function, std::chrono::steady_clock::now(), line}
    );
}

void TimeTracker::popTimer(std::string file, std::string function, int line)
{
    auto timer = m_timeStamps.top();
    m_timeStamps.pop();

    auto now = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - timer.now);
    std::cerr << "[" << timer.file << " in " << timer.function << " at " << timer.line << "]" << std::endl
              << "[" << file << " in " << function << " at " << line << "]: "  << elapsed.count() << " milis.";
}
