//
// Created by gdhsnlvr on 04.10.16.
//

#ifndef GEOLOGYWORKPROJECT_TIMETRACKER_H
#define GEOLOGYWORKPROJECT_TIMETRACKER_H

#include <string>
#include <chrono>
#include <stack>

class TimeTracker
{
protected:
    using time_point = std::chrono::steady_clock::time_point;

    struct TimeStamp {
        std::string file, function;
        time_point now;
        int line;
    };

    std::stack<TimeStamp> m_timeStamps;

public:
    TimeTracker() = default;
    static TimeTracker &get();

    void pushTimer(std::string file, std::string function, int line);
    void popTimer(std::string file, std::string function, int line);
};

#define TIME_COUNTER_START() TimeTracker::get().pushTimer(__FILE__, __FUNCTION__, __LINE__)
#define TIME_COUNTER_STOP() TimeTracker::get().popTimer(__FILE__, __FUNCTION__, __LINE__)

#endif //GEOLOGYWORKPROJECT_TIMETRACKER_H
