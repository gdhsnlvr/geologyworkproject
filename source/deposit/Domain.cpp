//
// Created by gdhsnlvr on 25.09.16.
//

#include "Domain.h"

Domain::Domain()
        : m_heightMin(0), m_heightMax(0), m_quality(0)
{
    m_parent = nullptr;
}

Domain::Domain(real_t heightMin, real_t heightMax, real_t quality, const geometry::polygon &polygon)
        : m_heightMin(heightMin), m_heightMax(heightMax), m_quality(quality), m_polygon(polygon)
{
    m_parent = nullptr;
}

void Domain::setQuality(real_t quality)
{
    m_quality = quality;
}

real_t Domain::quality() const
{
    return m_quality;
}

void Domain::setHeightMin(real_t heightMin)
{
    m_heightMin = heightMin;
}

real_t Domain::heightMin() const
{
    return m_heightMin;
}

void Domain::setHeightMax(real_t heightMax)
{
    m_heightMax = heightMax;
}

real_t Domain::heightMax() const
{
    return m_heightMax;
}

void Domain::setMesh(const geometry::mesh &mesh)
{
    m_mesh = mesh;
}

const geometry::mesh &Domain::mesh() const
{
    return m_mesh;
}

void Domain::setPolygon(const geometry::polygon &polygon)
{
    m_polygon = polygon;
}

const geometry::polygon &Domain::polygon() const
{
    return m_polygon;
}

Region *Domain::parent()
{
    return m_parent;
}

const Region *Domain::parent() const
{
    return m_parent;
}

void Domain::setParent(Region *parent)
{
    m_parent = parent;
}
