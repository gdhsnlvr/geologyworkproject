//
// Created by gdhsnlvr on 25.09.16.
//

#include <algorithm>
#include <source/geometry/detail/geometry_detail.h>
#include "Deposit.h"

Deposit::Deposit()
{
}

Deposit::Deposit(const geometry::mesh &mesh, const std::vector<real_t> &height)
{
    m_surface = mesh;
    for (int i = 1; i < height.size(); i++) {
        Horizon *horizon = new Horizon(height[i-1], height[i]);
        horizon->setMesh(geometry::detail::slice_mesh(mesh, height[i-1], height[i]));
        addHorizon(horizon);
    }
}

Deposit::~Deposit()
{
    for (Horizon *horizon : m_horizons)
        delete horizon;
}

void Deposit::setSurface(const geometry::mesh &surface)
{
    m_surface = surface;
}

const geometry::mesh &Deposit::surface() const
{
    return m_surface;
}

void Deposit::addHorizon(Horizon *horizon)
{
    m_horizons.push_back(horizon);
}

void Deposit::removeHorizon(Horizon *horizon)
{
    auto it = std::find(m_horizons.begin(), m_horizons.end(), horizon);
    if (it != m_horizons.end()) {
        m_horizons.erase(it);
    }
}

const std::vector<Horizon *> &Deposit::horizons() const
{
    return m_horizons;
}
