//
// Created by gdhsnlvr on 25.09.16.
//

#ifndef GEOLOGYWORKPROJECT_DEPOSIT_H
#define GEOLOGYWORKPROJECT_DEPOSIT_H


#include <source/geometry/mesh.h>
#include "Horizon.h"

class Deposit
{
protected:
    geometry::mesh m_surface;
    std::vector<Horizon*> m_horizons;

public:
    Deposit();
    Deposit(const geometry::mesh &mesh, const std::vector<real_t> &height);
    ~Deposit();

    void setSurface(const geometry::mesh &surface);
    const geometry::mesh &surface() const;

    void addHorizon(Horizon *horizon);
    void removeHorizon(Horizon *horizon);
    const std::vector<Horizon*> &horizons() const;
};


#endif //GEOLOGYWORKPROJECT_DEPOSIT_H
