//
// Created by gdhsnlvr on 25.09.16.
//

#ifndef GEOLOGYWORKPROJECT_DEPOSIT_IO_H
#define GEOLOGYWORKPROJECT_DEPOSIT_IO_H


#include <fstream>
#include <source/deposit/Deposit.h>
#include <source/geometry/io/detail/geometry_io_detail.h>

namespace deposit
{
    namespace io
    {
        void read(Deposit *deposit, const std::string &fileName)
        {
            std::ofstream ofs(fileName, std::ios_base::binary);
            for (Horizon *horizon : deposit->horizons()) {
                geometry::io::detail::write(horizon->mesh(), ofs);
            }
        }
    }
}


#endif //GEOLOGYWORKPROJECT_DEPOSIT_IO_H
