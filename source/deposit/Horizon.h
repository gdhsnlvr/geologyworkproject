//
// Created by gdhsnlvr on 25.09.16.
//

#ifndef GEOLOGYWORKPROJECT_HORIZON_H
#define GEOLOGYWORKPROJECT_HORIZON_H


#include <source/geometry/Real.hpp>
#include <source/geometry/mesh.h>
#include "Region.h"

class Horizon
{
protected:
    real_t m_heightMin;
    real_t m_heightMax;
    geometry::mesh m_mesh;
    std::vector<Region*> m_regions;

public:
    Horizon(real_t heightMin, real_t heightMax);
    ~Horizon();

    void setHeightMin(real_t heightMin);
    real_t heightMin() const;

    void setHeightMax(real_t heightMax);
    real_t heightMax() const;

    void setMesh(const geometry::mesh &mesh);
    const geometry::mesh &mesh() const;

    void addRegion(Region *region);
    void removeRegion(Region *region);
    const std::vector<Region*> &regions() const;
};


#endif //GEOLOGYWORKPROJECT_HORIZON_H
