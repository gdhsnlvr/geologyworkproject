//
// Created by gdhsnlvr on 25.09.16.
//

#include <algorithm>
#include "Region.h"

Region::Region(const std::vector<Domain *> domains)
{
    m_domains = domains;
}

Region::~Region()
{
    for (Domain *domain : m_domains)
        delete domain;
}

void Region::addDomain(Domain *domain)
{
    domain->setParent(this);
    m_domains.push_back(domain);
}

void Region::removeDomain(Domain *domain)
{
    auto it = std::find(m_domains.begin(), m_domains.end(), domain);
    if (it != m_domains.end()) {
        domain->setParent(nullptr);
        m_domains.erase(it);
    }
}

const std::vector<Domain *> &Region::domains() const
{
    return m_domains;
}

std::vector<Domain *> &Region::r_domains()
{
    return m_domains;
}

Horizon *Region::parent()
{
    return m_parent;
}

const Horizon *Region::parent() const
{
    return m_parent;
}

void Region::setParent(Horizon *parent)
{
    m_parent = parent;
}
