//
// Created by gdhsnlvr on 25.09.16.
//

#include <algorithm>
#include "Horizon.h"

Horizon::Horizon(real_t heightMin, real_t heightMax)
{
    m_heightMin = heightMin;
    m_heightMax = heightMax;
}

Horizon::~Horizon()
{
    for (Region *region : m_regions)
        delete region;
}

void Horizon::setHeightMin(real_t heightMin)
{
    m_heightMin = heightMin;
}

real_t Horizon::heightMin() const
{
    return m_heightMin;
}

void Horizon::setHeightMax(real_t heightMax)
{
    m_heightMax = heightMax;
}

real_t Horizon::heightMax() const
{
    return m_heightMax;
}

void Horizon::setMesh(const geometry::mesh &mesh)
{
    m_mesh = mesh;
}

const geometry::mesh &Horizon::mesh() const
{
    return m_mesh;
}

void Horizon::addRegion(Region *region)
{
    region->setParent(this);
    m_regions.push_back(region);
}

void Horizon::removeRegion(Region *region)
{
    auto it = std::find(m_regions.begin(), m_regions.end(), region);
    if (it != m_regions.end()) {
        region->setParent(nullptr);
        m_regions.erase(it);
    }
}

const std::vector<Region *> &Horizon::regions() const
{
    return m_regions;
}
